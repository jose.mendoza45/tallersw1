/**
 * @file EXERCISE01.cpp
 * @author Jose Alexis Mendoza Porras (alexismendozaporras@gmail.com)
 * @brief File description
Description Exercise01
Elabore un algoritmo que dados como datos  de entrada 
     el radio y la altura de un cilindro calcular, 
     el �rea lateral y el volumen del cilindro
 * @version 1.0
 * @date 04.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/

#define PI 3.14		// Constante Pi
		

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	// Declaration and Initialization of variables
	float terminalInputRadius = 0.0;			//Entrada de altura a traves del terminal
	float terminalInputHeight = 0.0;			//Entrada de radio a traves del terminal
	double circularArea = 0.0;				//Area circular calculada
	double lateralAreaCylinder = 0.0;		//El area lateral del cilindro calculada
	double volumeCylinder = 0.0;				//El volumen del cilindro calculada
	
	
	// Ask data throught the terminal
	cout<<"======== Insert Data ========";
	cout<<"\r\nWrite the radius of the cylinder: ";
	cin>>terminalInputRadius;
	cout<<"\r\nWrite the height of the cylinder: ";
	cin>>terminalInputHeight;
	
	// Calculation
	circularArea = PI * pow(terminalInputRadius,2.0);									//area_circulo = pi * radio^2
	lateralAreaCylinder = 2.0 * PI * terminalInputRadius * terminalInputHeight;			//area lateral = 2 * pi * radio * h
	volumeCylinder = PI*pow(terminalInputRadius,2.0)*terminalInputHeight;	     		//volumen=area_circulo * altura
	
	// Show results in terminal
	cout<<"\r\n======== Show Results =========\r\n";
	cout<<"\tThe lateral area is: "<<lateralAreaCylinder<<"\r\n";
	cout<<"\tThe volume is: "<<volumeCylinder<<"\r\n";
	
	 
	return 0;
		
}
	
