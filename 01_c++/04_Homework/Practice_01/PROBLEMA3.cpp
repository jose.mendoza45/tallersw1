/**
 * @file EXERCISE01.cpp
 * @author Jose Alexis Mendoza Porras (alexismendozaporras@gmail.com)
 * @brief File description
Description Exercise03
Queremos conocer los datos estad�sticos de una asignatura, por lo tanto, necesitamos un algoritmo 
que lea el n�mero de desaprobados, aprobados, notables y sobresalientes de una asignatura, y nos devuelva:
a. El tanto por ciento de alumnos que han superado la asignatura.
b. El tanto por ciento de desaprobados, aprobados, notables y sobresalientes de la asignatura.
 * @version 1.0
 * @date 04.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;
	
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	//Declaration and Initialization of variables
	int inputNumberDisapproved = 0;			//Entrada del numero de desaprobados de la asignatura a traves del terminal
	int inputNumberApproved = 0;			//Entrada del numero de aprobados de la asignatura a traves del terminal
	int inputNumberNotables = 0;			//Entrada del numero de notables de la asignatura a traves del terminal
	int inputNumberOutstandings = 0;		//Entrada del numero de sobresalientes a traves del terminal
	int numberTotal = 0;					//El numero total de notas de la asignatura
	
	float percentageDisapproved = 0.0;		//El porcentaje de desaprobados de la asignatura
	float percentageApproved = 0.0;			//El porcentaje de aprobados de la asignatura
	float percentageNotables = 0.0;			//El porcentaje de notables de la asignatura
	float percentageOutstandings = 0.0;		//El porcentaje de sobresalientes de la asignatura
	float percentageSuperior = 0.0;			//El porcentaje de los alumnos que superaron la asignatura
	
	// Ask data throught the terminal
	cout<<"======== Insert Data ========";
	cout<<"\r\nIngrese la cantidad de desaprobados: ";
	cin>>inputNumberDisapproved;
	cout<<"\r\nIngrese la cantidad de aprobados: ";
	cin>>inputNumberApproved;
	cout<<"\r\nIngrese la cantidad de notables: ";
	cin>>inputNumberNotables;
	cout<<"\r\nIngrese la cantidad de sobresalientes: ";
	cin>>inputNumberOutstandings;
	
	// Calculation NumberTotal
	numberTotal= inputNumberDisapproved+inputNumberApproved+inputNumberNotables+inputNumberOutstandings;
	
	// Calculation Percentages
	percentageSuperior = (float)(inputNumberApproved+inputNumberNotables+inputNumberOutstandings)*100.0/(float)numberTotal;
	percentageDisapproved = (float)inputNumberDisapproved*100.0/(float)numberTotal;
	percentageApproved = (float)inputNumberApproved*100.0/(float)numberTotal;
	percentageNotables = (float)inputNumberNotables*100.0/(float)numberTotal;
	percentageOutstandings = (float)inputNumberOutstandings*100.0/(float)numberTotal;
	
	// Show result in terminal
	cout<<"\r\n======== Show Results ========\r\n";
	cout<<"\tEl porcentaje de alumnos que superaron la asignatura es: "<<percentageSuperior<<"%\r\n";
	cout<<"\tEl porcentaje de desaprobados es: "<<percentageDisapproved<<"%\r\n";
	cout<<"\tEl porcentaje de aprobados es: "<<percentageApproved<<"%\r\n";
	cout<<"\tEl porcentaje de notables es: "<<percentageNotables<<"%\r\n";
	cout<<"\tEl porcentaje de sobresalientes es: "<<percentageOutstandings<<"%\r\n";
	
	
	return 0;

	
}
