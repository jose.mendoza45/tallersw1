
/**
 * @file EXERCISE02.cpp
 * @author Jose Alexis Mendoza Porras (alexismendozaporas@gmail.com)
 * @brief File description
 Description Exercise02
	Un maestro desea saber que porcentaje de hombres 
	y que porcentaje de mujeres hay en un grupo de estudiantes.
 * @version 1.0
 * @date 04.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;



/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/



int main(){
	
	// Declaration and Initialization of variables
	int inputNumberofMalestudents = 0;			//Entrada del numero de estudiantes varones a traves del terminal
	int inputNumberofFemalestudents = 0;		//Entrada del numero de estudiantes mujeres a traves del terminal
	int totalStudents = 0.0;					//El numero total de estudiantes pertenecientes al grupo

	double percentagenumberMalestudents = 0.0;			//El porcentaje de estudiantes varones
	double percentagenumberFemalestudents = 0.0;		//El porcentaje de estudiantes mujeres 

	// Ask data trought the terminal
	cout<<"======== Insert Data ========";
	cout<<"\r\nWrite the number of female students: ";
	cin>>numberofFemalestudents;
	cout<<"\r\nWrite the number of male students: ";
	cin>>numberofMalestudents;
	  
	// Total students	  
	totalStudents = numberofMalestudents + numberofFemalestudents;
	
	// Percentage of students
	percentagenumberMalestudents = (float)numberofMalestudents*100.0/(float)totalStudents;
	percentagenumberFemalestudents = (float)numberofFemalestudents*100.0/(float)totalStudents;
	
	// Show results in terminal
	cout<<"\r\n======== Show Results ========\r\n";
	cout<<"\tThe percentage of male students is: "<<percentagenumberMalestudents<<"%\r\n";
	cout<<"\tThe percentage of female students is: "<<percentagenumberFemalestudents<<"%\r\n";
	   
	return 0;
		
		
	}
