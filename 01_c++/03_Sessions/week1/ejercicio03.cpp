#include <iostream>

using namespace std;

/*
Function example 3
*/
int main(){
	
	//Declaration of example 03
	int numberInput;
	int resultSum;			
	int resultMulti;
	int resultRest;
		
	
	//Initialize
	numberInput = 0;
	resultSum =0;					
	resultMulti =0;
	resultRest = 0;
	
	//Display phrase 1
	cout<<"Escriba un numero entero : ";
	cin>>numberInput;
	
	//Operador
	resultSum = numberInput+1;
	resultMulti = numberInput*2;
	resultRest = numberInput-1;
	
	//Display phrase 2 with number
	cout<<"El reseultado de la suma es :"<<resultSum;
	cout<<"\n\El resultado de la multiplicacion es :"<<resultMulti;
	cout<<"\n\El resultado de la resta es :"<<resultRest;
		

	return 0;
	
}
