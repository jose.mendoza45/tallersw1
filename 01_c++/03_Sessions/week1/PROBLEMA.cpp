/*Include*/
#include <iostream>
using namespace std;
/*
Description del problema 3:
Queremos conocer los datos estad�sticos de una asignatura, por lo tanto, necesitamos un algoritmo 
que lea el n�mero de desaprobados, aprobados, notables y sobresalientes de una asignatura, y nos devuelva:
a. El tanto por ciento de alumnos que han superado la asignatura.
b. El tanto por ciento de desaprobados, aprobados, notables y sobresalientes de la asignatura.
*/
int main(){
	
	//Declaration and Initialize
	int numberDisapproved= 0.0;		//El numero de desaprobados de la asignatura
	int numberApproved= 0.0;		//El numero de aprobados de la asignatura
	int numberNotables= 0.0;		//El numero de notables de la asignatura
	int numberOutstandings= 0.0;		//El numero de sobresalientes
	int numberTotal= 0.0;		//El numero total de notas de la asignatura
	
	float percentageDisapproved= 0.0;		//El porcentaje de desaprobados de la asignatura
	float percentageApproved= 0.0;		//El porcentaje de aprobados de la asignatura
	float percentageNotables= 0.0;		//El porcentaje de notables de la asignatura
	float percentageOutstandings= 0.0;		//El porcentaje de sobresalientes de la asignatura
	float percentageSuperior= 0.0;		//El porcentaje de los alumnos que superaron la asignatura
	
	//Display phrase 1
	cout<<"Ingrese la cantidad de desaprobados ","/r/n";
	cin>>numberDisapproved;
	cout<<"Ingrese la cantidad de aprobados ","/r/n";
	cin>>numberApproved;
	cout<<"Ingrese la cantidad de notables ","/r/n";
	cin>>numberNotables;
	cout<<"Ingrese la cantidad de sobresalientes ","/r/n";
	cin>>numberOutstandings;
	
	//NumberTotal
	numberTotal= numberDisapproved+numberApproved+numberNotables+numberOutstandings;
	
	//Percentages
	percentageSuperior= (numberApproved+numberNotables+numberOutstandings)*100.0/numberTotal;
	percentageDisapproved= numberDisapproved*100.0/numberTotal;
	percentageApproved= numberApproved*100.0/numberTotal;
	percentageNotables= numberNotables*100.0/numberTotal;
	percentageOutstandings= numberOutstandings*100.0/numberTotal;
	
	//Display phrase 2
	cout<<"El porcentaje de alumnos que superaron la asignatura es: "<<percentageSuperior<<"%","/r/n";
	cout<<"\n\rEl porcentaje de desaprobados es: "<<percentageDisapproved<<"%","/r/n";
	cout<<"\n\rEl porcentaje de aprobados es: "<<percentageApproved<<"%","/r/n";
	cout<<"\n\rEl porcentaje de notables es: "<<percentageNotables<<"%","/r/n";
	cout<<"\n\rEl porcentaje de sobresalientes es: "<<percentageOutstandings<<"%","/r/n";
	
	
	return 0;

	
}
